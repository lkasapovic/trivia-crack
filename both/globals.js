Game = new Mongo.Collection('games');

UsersIndex = new EasySearch.Index({
    collection: Meteor.users,
    field: ['username'],
    engine: new EasySearch.Minimongo()
});